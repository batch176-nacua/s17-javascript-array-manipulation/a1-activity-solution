let students = []

function addStudent(name){
	students.push(name)
	console.log(name + " was added to the student's list.")
}

function countStudents(){
	console.log('There are a total of '+students.length+' students enrolled.')
}

function printStudents(){
	students.sort()
	students.forEach(function(show){
		console.log(show)
	})
}

function findStudent(searchKeyword){
	let filterFound = students.filter(function(found){
		return found.toLowerCase().includes(searchKeyword.toLowerCase());
	})

	filterFound.toString()
	if(filterFound.length == 1){
		console.log(filterFound + ' is an enrollee')
	}else if(filterFound.length > 1){
		console.log(filterFound + ' are enrollees')
	}else{
		console.log("No student found with the name " +searchKeyword)
	}
}

function addSection(section){
	section = students.map(function(name){
		return name + ' - section ' + section.toUpperCase();
	})
	console.log(section)
}

function removeStudent(name){
	let formalName = name.charAt(0).toUpperCase() + name.slice(1)
	let index = students.indexOf(formalName)

	if(index != -1){
		students.splice(index, 1)
		console.log(formalName + " was removed from the student's list.")
	}else{
		console.log(formalName + " is not found in the student's list.")
	}
}


